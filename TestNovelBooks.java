package NovelBooks;

public class TestNovelBooks {
    public static void main(String[] args) {
        NovelBooks NovI = new NovelBooks("HairPott", "Mookkkoom", "Saloth", 129.00, 150);
        NovI.showDescription();
        NovI.sell(7);
        NovI.printDataMovement();
        NovI.addStock(100);
        NovI.printDataMovement();
        NovI.sell(3);
        NovI.printDataMovement();
        NovI.addStock(-1);
        NovI.printDataMovement();
        NovI.setQuantity(-22);
        NovI.printDataMovement();

        System.out.println();

        NovelBooks NovII = new NovelBooks("Sherlhol", "Koomm", "tholas", 322.00, 99);
        NovII.showDescription();
        NovII.addStock(200);
        NovII.printDataMovement();
        NovII.sell(-3);
        NovII.printDataMovement();
        NovII.sell(15);
        NovII.printDataMovement();
        NovII.setPrice(-333);
        NovII.printDataMovement();
    }
}
