package NovelBooks;

public class NovelBooks {
    public String nameNovel;
    public String penname;
    public String namePublisher;
    private double price;
    private int quantity;
    private int sellTotal = 0;

    public NovelBooks(String nameNovel, String penname, String namePublisher, double price, int quantity){
        this.nameNovel = nameNovel;
        this.penname = penname;
        this.namePublisher = namePublisher;
        this.price = price;
        this.quantity = quantity;
    }

    public void addStock(int num){
        if(num < 0){
            System.out.println("Quantity of add books must not less than 0");
            return;
        }
        quantity += num;
    }

    public void sell(int num){
        if(num < 0){
            System.out.println("Quantity of sell books must not less than 0");
            return;
        }
        sellTotal += num;
        quantity -= num;
    }

    public double totalPriceSell(){
        return sellTotal*price;
    }

    public void showDescription(){
        System.out.println("Name of Book : " + nameNovel);
        System.out.println("Penname : " + penname);
        System.out.println("Name of Publisher : " + namePublisher);
        System.out.printf("Price of Book : %.2f", price);
        System.out.println("\n-----");
    }

    public void printDataMovement() {
        System.out.println("Quantity : " + quantity);
        System.out.printf("Total Income of these Books : %.2f\n", totalPriceSell());
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if(price < 0){
            System.out.println("Price must not less than 0");
            return;
        }
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        if(quantity < 0){
            System.out.println("Quantity must not less than 0");
            return;
        }
        this.quantity = quantity;
    }
}